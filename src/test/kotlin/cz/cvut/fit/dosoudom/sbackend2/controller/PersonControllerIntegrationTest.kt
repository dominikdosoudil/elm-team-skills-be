package cz.cvut.fit.dosoudom.sbackend2.controller

import cz.cvut.fit.dosoudom.sbackend2.dto.CreatePersonDTO
import cz.cvut.fit.dosoudom.sbackend2.dto.CreateSkillDTO
import cz.cvut.fit.dosoudom.sbackend2.service.PersonService
import cz.cvut.fit.dosoudom.sbackend2.service.SkillService
import org.junit.jupiter.api.*
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertTrue
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.web.client.TestRestTemplate
import org.springframework.boot.web.server.LocalServerPort
import org.springframework.http.*
import org.springframework.test.annotation.DirtiesContext

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@TestMethodOrder(MethodOrderer.OrderAnnotation::class)
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_CLASS)
class PersonControllerIntegrationTest (
        @Autowired val restTemplate: TestRestTemplate,
        @LocalServerPort val serverPort: Int,
) {

    companion object {
        @BeforeAll
        @JvmStatic
        fun loadDataIntoDb(@Autowired personService: PersonService, @Autowired skillService: SkillService) {
            personService.create(CreatePersonDTO("A", listOf()))
            personService.create(CreatePersonDTO("B", listOf()))
            personService.create(CreatePersonDTO("C", listOf()))
            skillService.create(CreateSkillDTO("Mining"))
            skillService.create(CreateSkillDTO("Farming"))
            skillService.create(CreateSkillDTO("Fighting"))
            personService.create(CreatePersonDTO("D", listOf(4, 5)))
            personService.create(CreatePersonDTO("E", listOf(4)))
            personService.create(CreatePersonDTO("F", listOf(5, 6)))
        }

        const val URL = "/persons"
    }

    @Test
    @Order(10)
    fun `lists persons`() {
        restTemplate.getForEntity("$URL/", String::class.java).let {
            assertEquals(HttpStatus.OK, it.statusCode)
            assertEquals(
                "[" +
                "{\"id\":1,\"name\":\"A\",\"skillIds\":[]}," +
                "{\"id\":2,\"name\":\"B\",\"skillIds\":[]}," +
                "{\"id\":3,\"name\":\"C\",\"skillIds\":[]}," +
                "{\"id\":7,\"name\":\"D\",\"skillIds\":[4,5]}," +
                "{\"id\":8,\"name\":\"E\",\"skillIds\":[4]}," +
                "{\"id\":9,\"name\":\"F\",\"skillIds\":[5,6]}" +
                "]",
                it.body,
            )
        }
    }

    @Test
    @Order(15)
    fun `list persons that posses all needed skills`() {
        restTemplate.getForEntity("$URL/?skillIds=4", String::class.java).let {
            assertEquals(HttpStatus.OK, it.statusCode)
            assertEquals(
                "[" +
                    "{\"id\":7,\"name\":\"D\",\"skillIds\":[4,5]}," +
                    "{\"id\":8,\"name\":\"E\",\"skillIds\":[4]}" +
                    "]",
                it.body,
            )
        }
        restTemplate.getForEntity("$URL/?skillIds=5,4", String::class.java).let {
            assertEquals(HttpStatus.OK, it.statusCode)
            assertEquals(
                "[" +
                    "{\"id\":7,\"name\":\"D\",\"skillIds\":[4,5]}" +
                    "]",
                it.body,
            )
        }
        restTemplate.getForEntity("$URL/?skillIds=", String::class.java).let {
            assertEquals(HttpStatus.OK, it.statusCode)
            assertEquals(
                "[" +
                    "{\"id\":1,\"name\":\"A\",\"skillIds\":[]}," +
                    "{\"id\":2,\"name\":\"B\",\"skillIds\":[]}," +
                    "{\"id\":3,\"name\":\"C\",\"skillIds\":[]}," +
                    "{\"id\":7,\"name\":\"D\",\"skillIds\":[4,5]}," +
                    "{\"id\":8,\"name\":\"E\",\"skillIds\":[4]}," +
                    "{\"id\":9,\"name\":\"F\",\"skillIds\":[5,6]}" +
                    "]",
                it.body,
            )
        }
    }

    @Test
    @Order(20)
    fun `find existing person`() {
        restTemplate.getForEntity("$URL/{id}", String::class.java, 2).let {
            assertEquals(HttpStatus.OK, it.statusCode)
            assertEquals("{\"id\":2,\"name\":\"B\",\"skillIds\":[]}", it.body)
        }
    }

    @Test
    @Order(30)
    fun `find nonexisting person`() {
        restTemplate.getForEntity("$URL/{id}", String::class.java, 20).let {
            assertEquals(HttpStatus.NOT_FOUND, it.statusCode)
        }
    }

    @Test
    @Order(40)
    fun `create person`() {
        val h = HttpHeaders()
        h.contentType = MediaType.APPLICATION_JSON
        val r = HttpEntity("{\"name\":\"D\",\"skillIds\":[]}", h)
        restTemplate.postForEntity(URL, r, String::class.java).let {
            assertEquals(HttpStatus.CREATED, it.statusCode)
            assertTrue(it.headers.containsKey("Location"))
            assertEquals("[http://localhost:$serverPort/persons/10]", it.headers["Location"]!!.toString())
            assertEquals("{\"id\":10,\"name\":\"D\",\"skillIds\":[]}", it.body)
        }
    }

    @Test
    @Order(50)
    fun `create person input sanitization`() {
        assertEquals(
            HttpStatus.UNSUPPORTED_MEDIA_TYPE,
            restTemplate
                .postForEntity(URL, "abc", String::class.java)
                .statusCode,
        )
    }

    @Test
    @Order(60)
    fun `delete person`() {
        assertEquals(
            HttpStatus.OK,
            restTemplate
                .exchange(
                    "$URL/{id}",
                    HttpMethod.DELETE,
                    HttpEntity.EMPTY,
                    String::class.java,
                    3,
                )
                .statusCode,
        )
    }

    @Test
    @Order(70)
    fun `delete person returns NOT FOUND`() {
        assertEquals(
            HttpStatus.NOT_FOUND,
            restTemplate
                .exchange(
                    "$URL/{id}",
                    HttpMethod.DELETE,
                    HttpEntity.EMPTY,
                    String::class.java,
                    404,
                )
                .statusCode,
        )
    }

    @Test
    @Order(80)
    fun `update person name`() {
        val personId = 2
        val h = HttpHeaders()
        h.contentType = MediaType.APPLICATION_JSON
        val r = HttpEntity("\"X\"", h)
        restTemplate.exchange("$URL/{id}", HttpMethod.PUT, r, String::class.java, personId).let {
            assertTrue(it.headers.containsKey("Location"))
            assertEquals("[http://localhost:$serverPort/persons/2]", it.headers["Location"]!!.toString())
            assertEquals(HttpStatus.CREATED, it.statusCode)
        }

        // assure that it was changed
        restTemplate.getForEntity("$URL/{id}", String::class.java, personId).let {
            assertEquals(HttpStatus.OK, it.statusCode)
            assertEquals("{\"id\":2,\"name\":\"X\",\"skillIds\":[]}", it.body)
        }
    }

    @Test
    @Order(85)
    fun`update person that does not exist`() {
        val personId = 404
        val h = HttpHeaders()
        h.contentType = MediaType.APPLICATION_JSON
        val r = HttpEntity("\"X\"", h)
        assertEquals(
            HttpStatus.NOT_FOUND,
            restTemplate.exchange("$URL/{id}", HttpMethod.PUT, r, String::class.java, personId).statusCode,
        )
    }

    @Test
    @Order(90)
    fun `delete range of items`() {
        assertEquals(
            HttpStatus.OK,
            restTemplate
                .exchange("$URL/{idFrom}/{idTo}", HttpMethod.DELETE, HttpEntity.EMPTY, String::class.java, 1, 3)
                .statusCode,
        )
    }

    @Test
    @Order(95)
    fun `add skill to person`() {
        restTemplate
            .postForEntity("$URL/{id}/skills", 5, String::class.java, 10)
            .let {
                assertEquals(HttpStatus.CREATED, it.statusCode)
                assertEquals("{\"id\":10,\"name\":\"D\",\"skillIds\":[5]}", it.body)
            }
        restTemplate
            .getForEntity("$URL/{id}/skills/{skillId}", String::class.java, 10, 5)
            .let {
                assertEquals(HttpStatus.OK, it.statusCode)
                assertEquals("true", it.body)
            }
    }



    @Test
    @Order(96)
    fun `adding duplicate skill to person adds nothing`() {
        restTemplate
            .postForEntity("$URL/{id}/skills", 5, String::class.java, 10)
            .let {
                assertEquals(HttpStatus.CREATED, it.statusCode)
                assertEquals("{\"id\":10,\"name\":\"D\",\"skillIds\":[5]}", it.body)
            }
    }

    @Test
    @Order(97)
    fun `add nonexisting skill to person`() {
        restTemplate
            .postForEntity("$URL/{id}/skills", 404, String::class.java, 10)
            .let {
                assertEquals(HttpStatus.NOT_FOUND, it.statusCode)
            }
    }

    @Test
    @Order(97)
    fun `add skill to nonexisting person`() {
        restTemplate
            .postForEntity("$URL/{id}/skills", 5, String::class.java, 404)
            .let {
                assertEquals(HttpStatus.NOT_FOUND, it.statusCode)
            }
    }

    @Test
    @Order(98)
    fun `remove skill from person`() {
        val personId = 10
        val skillIdToDelete = 6
        restTemplate
            .postForEntity("$URL/{id}/skills", skillIdToDelete, String::class.java, personId)
        restTemplate
            .getForEntity("$URL/{id}/skills/{skillId}", String::class.java, personId, skillIdToDelete)
            .let {
                assertEquals(HttpStatus.OK, it.statusCode)
                assertEquals("true", it.body)
            }

        restTemplate.exchange(
            "$URL/{id}/skills/{skillId}",
            HttpMethod.DELETE,
            HttpEntity.EMPTY,
            String::class.java,
            personId,
            skillIdToDelete,
        )
        restTemplate
            .getForEntity("$URL/{id}/skills/{skillId}", String::class.java, personId, skillIdToDelete)
            .let {
                assertEquals(HttpStatus.OK, it.statusCode)
                assertEquals("false", it.body)
            }

    }

    @Test
    @Order(99)
    fun `remove skill that person does not posses`() {
        val personId = 10
        val skillIdToDelete = 7
        restTemplate
            .getForEntity("$URL/{id}/skills/{skillId}", String::class.java, personId, skillIdToDelete)
            .let {
                assertEquals(HttpStatus.OK, it.statusCode)
                assertEquals("false", it.body)
            }

        restTemplate.exchange(
            "$URL/{id}/skills/{skillId}",
            HttpMethod.DELETE,
            HttpEntity.EMPTY,
            String::class.java,
            personId,
            skillIdToDelete,
        )
        restTemplate
            .getForEntity("$URL/{id}/skills/{skillId}", String::class.java, personId, skillIdToDelete)
            .let {
                assertEquals(HttpStatus.OK, it.statusCode)
                assertEquals("false", it.body)
            }

    }

    @Test
    @Order(99)
    fun `remove skill from nonexisting person`() {
        val personId = 404
        val skillIdToDelete = 7

        restTemplate.exchange(
            "$URL/{id}/skills/{skillId}",
            HttpMethod.DELETE,
            HttpEntity.EMPTY,
            String::class.java,
            personId,
            skillIdToDelete,
        ).let {
            assertEquals(HttpStatus.NOT_FOUND, it.statusCode)
        }
    }
    @Test
    @Order(100)
    fun `nonexisting person posses skill`() {
        restTemplate
            .getForEntity("$URL/{id}/skills/{skillId}", String::class.java, 404, 10)
            .let {
                assertEquals(HttpStatus.NOT_FOUND, it.statusCode)
            }
    }

    @Test
    @Order(110)
    fun `check state of database after all tests`() {
        restTemplate.getForEntity(URL, String::class.java).let {
            assertEquals(HttpStatus.OK, it.statusCode)
            assertEquals("[" +
                "{\"id\":7,\"name\":\"D\",\"skillIds\":[4,5]}," +
                "{\"id\":8,\"name\":\"E\",\"skillIds\":[4]}," +
                "{\"id\":9,\"name\":\"F\",\"skillIds\":[5,6]}," +
                "{\"id\":10,\"name\":\"D\",\"skillIds\":[5]}" +
                "]", it.body)
        }
    }
}