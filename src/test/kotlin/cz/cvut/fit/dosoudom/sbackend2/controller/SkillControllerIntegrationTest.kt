package cz.cvut.fit.dosoudom.sbackend2.controller

import cz.cvut.fit.dosoudom.sbackend2.dto.CreatePersonDTO
import cz.cvut.fit.dosoudom.sbackend2.dto.CreateSkillDTO
import cz.cvut.fit.dosoudom.sbackend2.service.PersonService
import cz.cvut.fit.dosoudom.sbackend2.service.SkillService
import org.junit.jupiter.api.*
import org.junit.jupiter.api.Assertions.assertEquals
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.web.client.TestRestTemplate
import org.springframework.boot.web.server.LocalServerPort
import org.springframework.http.*
import org.springframework.test.annotation.DirtiesContext


@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@TestMethodOrder(MethodOrderer.OrderAnnotation::class)
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_CLASS)
class SkillControllerIntegrationTest(
    @Autowired val restTemplate: TestRestTemplate,
    @LocalServerPort val serverPort: Int,
) {
    companion object {
        @BeforeAll
        @JvmStatic
        fun loadDataIntoDb(@Autowired skillService: SkillService, @Autowired personService: PersonService) {
            skillService.create(CreateSkillDTO("Mining"))
            skillService.create(CreateSkillDTO("Digging"))
            skillService.create(CreateSkillDTO("Fighting"))
            skillService.create(CreateSkillDTO("Farming"))
            personService.create(CreatePersonDTO("Dhomnorlum Shatterdelver", listOf(1, 2)))
        }
        const val URL = "/skills"
    }

    @Test
    @Order(10)
    fun `lists skills`() {
        restTemplate.getForEntity(URL, String::class.java).let {
            assertEquals(HttpStatus.OK, it.statusCode)
            assertEquals(
                "[" +
                "{\"id\":1,\"name\":\"Mining\"}," +
                "{\"id\":2,\"name\":\"Digging\"}," +
                "{\"id\":3,\"name\":\"Fighting\"}," +
                "{\"id\":4,\"name\":\"Farming\"}" +
                "]",
                it.body,
            )
        }
    }

    @Test
    @Order(10)
    fun `lists skills by ids`() {
        restTemplate.getForEntity("$URL?ids=1,3", String::class.java).let {
            assertEquals(HttpStatus.OK, it.statusCode)
            assertEquals(
                "[" +
                "{\"id\":1,\"name\":\"Mining\"}," +
                "{\"id\":3,\"name\":\"Fighting\"}" +
                "]",
                it.body,
            )
        }
    }

    @Test
    @Order(20)
    fun `find existing skill`() {
        restTemplate.getForEntity("$URL/{id}", String::class.java, 3).let {
            assertEquals(HttpStatus.OK, it.statusCode)
            assertEquals("{\"id\":3,\"name\":\"Fighting\"}", it.body)
        }
    }

    @Test
    @Order(30)
    fun `find nonexisting skill`() {
        assertEquals(
            HttpStatus.NOT_FOUND,
            restTemplate.getForEntity("$URL/{id}", String::class.java, 42).statusCode
        )
    }

    @Test
    @Order(40)
    fun `create skill`() {
        val h = HttpHeaders()
        h.contentType = MediaType.APPLICATION_JSON
        val r = HttpEntity("\"Blowing things up\"", h)
        restTemplate.postForEntity(URL, r, String::class.java).let {
            assertEquals(HttpStatus.CREATED, it.statusCode)
            Assertions.assertTrue(it.headers.containsKey("Location"))
            assertEquals("[http://localhost:$serverPort$URL/6]", it.headers["Location"]!!.toString())
            assertEquals("{\"id\":6,\"name\":\"Blowing things up\"}", it.body)
        }
    }

    @Test
    @Order(50)
    fun `create skill input sanitization`() {
        assertEquals(
            HttpStatus.UNSUPPORTED_MEDIA_TYPE,
            restTemplate
                .postForEntity(URL, "abc", String::class.java)
                .statusCode,
        )
    }

    @Test
    @Order(55)
    fun `create skill with duplicate name`() {
        val h = HttpHeaders()
        h.contentType = MediaType.APPLICATION_JSON
        val r = HttpEntity("\"Blowing things up\"", h)
        assertEquals(
            HttpStatus.CONFLICT,
            restTemplate.postForEntity(URL, r, String::class.java).statusCode,
        )
    }

    @Test
    @Order(60)
    fun `delete skill`() {
        assertEquals(
            HttpStatus.OK,
            restTemplate
                .exchange(
                    "$URL/{id}",
                    HttpMethod.DELETE,
                    HttpEntity.EMPTY,
                    String::class.java,
                    3,
                )
                .statusCode,
        )
    }

    @Test
    @Order(70)
    fun `delete nonexisting skill returns NOT FOUND`() {
        assertEquals(
            HttpStatus.NOT_FOUND,
            restTemplate
                .exchange(
                    "$URL/{id}",
                    HttpMethod.DELETE,
                    HttpEntity.EMPTY,
                    String::class.java,
                    423,
                )
                .statusCode,
        )
    }

    @Test
    @Order(80)
    fun `update skill name`() {
        val skillId = 2
        val h = HttpHeaders()
        h.contentType = MediaType.APPLICATION_JSON
        val r = HttpEntity("\"Blacksmithing\"", h)
        restTemplate.exchange("$URL/{id}", HttpMethod.PUT, r, String::class.java, skillId).let {
            Assertions.assertTrue(it.headers.containsKey("Location"))
            assertEquals("[http://localhost:$serverPort$URL/$skillId]", it.headers["Location"]!!.toString())
            assertEquals(HttpStatus.CREATED, it.statusCode)
        }

        // assure that it was changed
        restTemplate.getForEntity("$URL/{id}", String::class.java, skillId).let {
            assertEquals(HttpStatus.OK, it.statusCode)
            assertEquals("{\"id\":2,\"name\":\"Blacksmithing\"}", it.body)
        }
    }

    @Test
    @Order(100)
    fun `update skill with duplicate name`() {
        val skillId = 2
        val h = HttpHeaders()
        h.contentType = MediaType.APPLICATION_JSON
        val r = HttpEntity("\"Mining\"", h)
        assertEquals(
            HttpStatus.CONFLICT,
            restTemplate
                .exchange("$URL/{id}", HttpMethod.PUT, r, String::class.java, skillId)
                .statusCode,
        )
    }

    @Test
    @Order(105)
    fun `update skill that does not exist`() {
        val skillId = 404
        val h = HttpHeaders()
        h.contentType = MediaType.APPLICATION_JSON
        val r = HttpEntity("\"X\"", h)
        assertEquals(
            HttpStatus.NOT_FOUND,
            restTemplate
                .exchange("$URL/{id}", HttpMethod.PUT, r, String::class.java, skillId)
                .statusCode,
        )
    }

    @Test
    @Order(110)
    fun `update skill with duplicate name but same id`() {
        val skillId = 1
        val h = HttpHeaders()
        h.contentType = MediaType.APPLICATION_JSON
        val r = HttpEntity("\"Mining\"", h)
        assertEquals(
            HttpStatus.CREATED,
            restTemplate
                .exchange("$URL/{id}", HttpMethod.PUT, r, String::class.java, skillId)
                .statusCode,
        )
    }

    @Test
    @Order(120)
    fun `deleting skill will also remove it from person`() {
        assertEquals(
            HttpStatus.OK,
            restTemplate
                .exchange(
                    "$URL/{id}",
                    HttpMethod.DELETE,
                    HttpEntity.EMPTY,
                    String::class.java,
                    1,
                )
                .statusCode,
        )
        restTemplate.getForEntity("/persons/{id}", String::class.java, 5).let {
            assertEquals(HttpStatus.OK, it.statusCode)
            assertEquals("{\"id\":5,\"name\":\"Dhomnorlum Shatterdelver\",\"skillIds\":[2]}", it.body)
        }

    }

    @Test
    @Order(200)
    fun `check state of database after all tests`() {
        restTemplate.getForEntity(URL, String::class.java).let {
            assertEquals(HttpStatus.OK, it.statusCode)
            assertEquals(
                "[" +
                    "{\"id\":2,\"name\":\"Blacksmithing\"}," +
                    "{\"id\":4,\"name\":\"Farming\"}," +
                    "{\"id\":6,\"name\":\"Blowing things up\"}" +
                    "]",
                it.body,
            )
        }
    }
}