package cz.cvut.fit.dosoudom.sbackend2.controller

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.web.client.TestRestTemplate
import org.springframework.http.HttpStatus
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders
import org.springframework.test.web.servlet.result.MockMvcResultMatchers

@AutoConfigureMockMvc
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class HomeControllerIntegrationTest(
        @Autowired val restTemplate: TestRestTemplate,
        @Autowired val mvc: MockMvc
) {

    @Test
    fun `Assert homepage test does nothing`() {
        mvc.perform(MockMvcRequestBuilders.get("/"))
                .andExpect(MockMvcResultMatchers.status().isOk)
                .andExpect(MockMvcResultMatchers.content().string("Hello world"))
    }

    @Test
    fun `Homepage does nothing somehow else`() {
        val entity = restTemplate.getForEntity("/", String::class.java)
        assertEquals(entity.statusCode, HttpStatus.OK)
        assertEquals(entity.body, "Hello world")
    }
}