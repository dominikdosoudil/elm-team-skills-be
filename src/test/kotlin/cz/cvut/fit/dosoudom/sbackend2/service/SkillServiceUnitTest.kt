package cz.cvut.fit.dosoudom.sbackend2.service

import cz.cvut.fit.dosoudom.sbackend2.dto.CreateSkillDTO
import cz.cvut.fit.dosoudom.sbackend2.dto.SkillDTO
import cz.cvut.fit.dosoudom.sbackend2.entity.Skill
import cz.cvut.fit.dosoudom.sbackend2.exception.ItemAlreadyExists
import cz.cvut.fit.dosoudom.sbackend2.exception.ItemNotFound
import cz.cvut.fit.dosoudom.sbackend2.repository.SkillRepository
import org.junit.jupiter.api.assertThrows
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test
import org.mockito.BDDMockito.*
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.mock.mockito.MockBean
import java.util.*

@SpringBootTest
class SkillServiceUnitTest {
    @Autowired
    lateinit var skillService: SkillService

    @MockBean
    lateinit var skillRepository: SkillRepository

    companion object {
        private val s1 = Skill("Mining", 1)
        private val s2 = Skill("Farming", 2)
        private val s3 = Skill("Fighting", 3)
        private val s4 = Skill("Magic", 4)
    }

    @Test
    fun `list skill`() {
        given(skillRepository.findAll()).willReturn(listOf(s1, s2, s3, s4))

        assertEquals(
            listOf(
                SkillDTO(s1.id!!, s1.name),
                SkillDTO(s2.id!!, s2.name),
                SkillDTO(s3.id!!, s3.name),
                SkillDTO(s4.id!!, s4.name)
            ),
            skillService.list(),
        )
    }

    @Test
    fun `list skills by ids`() {
        given(skillRepository.findAllById(listOf(3, 2))).willReturn(listOf(s2, s3))

        assertEquals(
            listOf(
                SkillDTO(s2.id!!, s2.name),
                SkillDTO(s3.id!!, s3.name),
            ),
            skillService.list(listOf(3, 2)),
        )
    }

    @Test
    fun `find skill`() {
        given(skillRepository.findById(2)).willReturn(Optional.of(s2))

        assertEquals(
            SkillDTO(s2.id!!, s2.name),
            skillService.get(2),
        )
    }

    @Test
    fun `find skill - nonexisting`() {
        given(skillRepository.findById(404)).willReturn(Optional.empty())

        assertEquals(null, skillService.get(404))
    }

    @Test
    fun `create skill`() {
        given(skillRepository.save(eq(Skill(s4.name)))).willReturn(s4)

        assertEquals(
            SkillDTO(s4.id!!, s4.name),
            skillService.create(CreateSkillDTO(s4.name)),
        )
    }

    @Test
    fun `update skill`() {
        given(skillRepository.findById(s2.id!!)).willReturn(Optional.of(s2))

        assertEquals(
            SkillDTO(s2.id!!, "Blowing things up"),
            skillService.update(s2.id!!, CreateSkillDTO("Blowing things up")),
        )
    }

    @Test
    fun `update skill that does not exist`() {
        given(skillRepository.findById(404)).willReturn(Optional.empty())

        assertThrows<ItemNotFound> {
            skillService.update(404, CreateSkillDTO("Blowing things up"))
        }
    }

    @Test
    fun `update skill with duplicate name`() {
        given(skillRepository.findByName(s2.name)).willReturn(s2)
        given(skillRepository.findById(s3.id!!)).willReturn(Optional.of(s3))

        assertThrows<ItemAlreadyExists> {
            skillService.update(s3.id!!, CreateSkillDTO(s2.name))
        }
    }

    @Test
    fun `update skill with duplicate name but same id`() {
        given(skillRepository.findByName(s2.name)).willReturn(s2)
        given(skillRepository.findById(s2.id!!)).willReturn(Optional.of(s2))

        assertEquals(
            SkillDTO(s2.id!!, s2.name),
            skillService.update(s2.id!!, CreateSkillDTO(s2.name)),
        )
    }

    @Test
    fun `delete skill that does not exist`() {
        given(skillRepository.findById(404)).willReturn(Optional.empty())

        assertThrows<ItemNotFound> {
            skillService.delete(404)
        }
    }
}