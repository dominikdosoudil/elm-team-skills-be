package cz.cvut.fit.dosoudom.sbackend2.controller

import cz.cvut.fit.dosoudom.sbackend2.dto.CreateSkillDTO
import cz.cvut.fit.dosoudom.sbackend2.dto.SkillDTO
import cz.cvut.fit.dosoudom.sbackend2.exception.ItemAlreadyExists
import cz.cvut.fit.dosoudom.sbackend2.exception.ItemNotFound
import cz.cvut.fit.dosoudom.sbackend2.service.SkillService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.hateoas.server.mvc.linkTo
import org.springframework.http.HttpStatus
import org.springframework.http.MediaType
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import org.springframework.web.server.ResponseStatusException

@RestController
@RequestMapping("/skills")
class SkillController(
    @Autowired
    val skillService: SkillService,
) : CRUDController<Long, SkillDTO, CreateSkillDTO, CreateSkillDTO> {
    @GetMapping
    override fun list(): List<SkillDTO> = skillService.list()

    @GetMapping(params = ["ids"])
    fun list(@RequestParam ids: List<Long>): List<SkillDTO> = skillService.list(ids)

    @GetMapping("/{id}")
    override fun get(@PathVariable id: Long): ResponseEntity<SkillDTO>
        = skillService.get(id)?.let { ResponseEntity.ok(it) }
        ?: throw ResponseStatusException(HttpStatus.NOT_FOUND)

    @PostMapping(consumes = [MediaType.APPLICATION_JSON_VALUE])
    override fun create(@RequestBody item: CreateSkillDTO): ResponseEntity<SkillDTO>
        = try {
            skillService.create(item).let {
                ResponseEntity
                    .created(linkTo<SkillController> { get(it.id) }.toUri())
                    .body(it)
            }
        } catch (_: ItemAlreadyExists) {
            throw ResponseStatusException(HttpStatus.CONFLICT)
        }

    @PutMapping("/{id}", consumes = [MediaType.APPLICATION_JSON_VALUE])
    override fun update(@PathVariable id: Long, @RequestBody item: CreateSkillDTO): ResponseEntity<SkillDTO>
        = try {
            skillService.update(id, item).let {
                ResponseEntity
                    .created(linkTo<SkillController> { get(it.id) }.toUri())
                    .body(it)
            }
        } catch (_: ItemNotFound) {
            throw ResponseStatusException(HttpStatus.NOT_FOUND)
        } catch (_: ItemAlreadyExists) {
            throw ResponseStatusException(HttpStatus.CONFLICT)
        }

    @DeleteMapping("/{id}")
    override fun delete(@PathVariable id: Long) {
        try {
            skillService.delete(id)
        } catch (_: ItemNotFound) {
            throw ResponseStatusException(HttpStatus.NOT_FOUND)
        }
    }
}