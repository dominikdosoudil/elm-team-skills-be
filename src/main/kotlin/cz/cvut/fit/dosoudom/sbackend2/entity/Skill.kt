package cz.cvut.fit.dosoudom.sbackend2.entity

import com.sun.istack.NotNull
import javax.persistence.*

@Entity
class Skill(
    @NotNull
    @Column(unique = true)
    var name: String,

    @Id
    @GeneratedValue
    val id: Long? = null,

    @ManyToMany(mappedBy = "skills")
    val persons: List<Person> = listOf()
) {
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as Skill

        if (name != other.name) return false
        if (id != other.id) return false
        if (persons != other.persons) return false

        return true
    }

    override fun hashCode(): Int {
        var result = name.hashCode()
        result = 31 * result + (id?.hashCode() ?: 0)
        result = 31 * result + persons.hashCode()
        return result
    }
}
