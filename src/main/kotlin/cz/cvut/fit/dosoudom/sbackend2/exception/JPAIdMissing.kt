package cz.cvut.fit.dosoudom.sbackend2.exception

import java.lang.Exception

class JPAIdMissing : Exception("Jpa did not provide id to entity")