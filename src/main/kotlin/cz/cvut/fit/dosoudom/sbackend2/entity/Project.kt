package cz.cvut.fit.dosoudom.sbackend2.entity

import com.sun.istack.NotNull
import com.sun.istack.Nullable
import javax.persistence.*

@Entity
class Project(
    @NotNull
    var name: String,

    @Nullable
    var manDayEstimate: Long? = null,

    @ManyToMany
    @JoinTable(
        name = "project_persons",
        joinColumns = [JoinColumn(name = "person_id")],
        inverseJoinColumns = [JoinColumn(name = "skill_id")]
    )
    var persons: List<Person> = listOf(),

    @Id
    @GeneratedValue
    val id: Long? = null,
) {
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as Project

        if (name != other.name) return false
        if (manDayEstimate != other.manDayEstimate) return false
        if (persons != other.persons) return false
        if (id != other.id) return false

        return true
    }

    override fun hashCode(): Int {
        var result = name.hashCode()
        result = 31 * result + (manDayEstimate?.hashCode() ?: 0)
        result = 31 * result + persons.hashCode()
        result = 31 * result + (id?.hashCode() ?: 0)
        return result
    }
}

