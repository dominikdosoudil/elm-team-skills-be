package cz.cvut.fit.dosoudom.sbackend2.controller

import cz.cvut.fit.dosoudom.sbackend2.dto.CreatePersonDTO
import cz.cvut.fit.dosoudom.sbackend2.dto.PersonDTO
import cz.cvut.fit.dosoudom.sbackend2.dto.UpdatePersonDTO
import cz.cvut.fit.dosoudom.sbackend2.exception.ItemNotFound
import cz.cvut.fit.dosoudom.sbackend2.service.PersonService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.hateoas.server.mvc.linkTo
import org.springframework.http.HttpStatus
import org.springframework.http.MediaType
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import org.springframework.web.server.ResponseStatusException

@RestController
@RequestMapping("/persons")
class PersonController(
    @Autowired
    val personService: PersonService,
) : CRUDController<Long, PersonDTO, CreatePersonDTO, UpdatePersonDTO> {
    @GetMapping
    override fun list(): Iterable<PersonDTO> = personService.list()

    @GetMapping(params = ["skillIds"])
    fun list(@RequestParam skillIds: List<Long>): List<PersonDTO> = personService.list(skillIds)

    @GetMapping("/{id}")
    override fun get(@PathVariable id: Long): ResponseEntity<PersonDTO>
        = personService.get(id)?.let { ResponseEntity.ok(it) }
        ?: throw ResponseStatusException(HttpStatus.NOT_FOUND)

    @PostMapping(consumes = [MediaType.APPLICATION_JSON_VALUE])
    override fun create(@RequestBody item: CreatePersonDTO): ResponseEntity<PersonDTO>
        = personService.create(item).let {
            ResponseEntity
                .created(linkTo<PersonController> { get(it.id) }.toUri())
                .body(it)
        }

    @PutMapping("/{id}", consumes = [MediaType.APPLICATION_JSON_VALUE])
    override fun update(@PathVariable id: Long, @RequestBody patch: UpdatePersonDTO): ResponseEntity<PersonDTO>
        = try {
            personService.update(id, patch).let {
                ResponseEntity
                    .created(linkTo<PersonController> { get(id) }.toUri())
                    .body(it)
            }
        } catch (_: ItemNotFound) {
            throw ResponseStatusException(HttpStatus.NOT_FOUND)
        }

    @DeleteMapping("/{id}")
    override fun delete(@PathVariable id: Long)
        = try {
            personService.delete(id)
        } catch (_: ItemNotFound) {
            throw ResponseStatusException(HttpStatus.NOT_FOUND)
        }

    @DeleteMapping("/{idFrom}/{idTo}")
    fun deleteRange(@PathVariable idFrom: Long, @PathVariable idTo: Long) {
        (idFrom..idTo).forEach {
            try {
                personService.delete(it)
            } catch (_: ItemNotFound) {
                // nvm, we skip what doesn't exist
            }
        }
    }

    // TODO: should return boolean or actual skill data?
    @GetMapping("/{id}/skills/{skillId}")
    fun hasSkill(@PathVariable id: Long, @PathVariable skillId: Long): Boolean = try {
        personService.hasSkill(id, skillId)
    } catch (_: ItemNotFound) {
        throw ResponseStatusException(HttpStatus.NOT_FOUND)
    }

    // Maybe there should be PatchMapping instead...
    @PostMapping("/{id}/skills")
    fun addSkill(@PathVariable id: Long, @RequestBody skillId: Long): ResponseEntity<PersonDTO> = try {
        personService.addSkill(id, skillId).let {
            ResponseEntity
                // TODO: I am not sure if this link is correct, it could be also /persons/{id}/skills/{id}
                //  however this resource doesn't exist for GET and also will not exist because it makes no sense
                .created(linkTo<PersonController> { get(id) }.toUri())
                .body(it)
        }
    } catch (_: ItemNotFound) {
        throw ResponseStatusException(HttpStatus.NOT_FOUND)
    }

    @DeleteMapping("/{id}/skills/{skillId}")
    fun removeSkill(@PathVariable id: Long, @PathVariable skillId: Long) = try {
        personService.removeSkill(id, skillId)
    } catch (_: ItemNotFound) {
        throw ResponseStatusException(HttpStatus.NOT_FOUND)
    }
}