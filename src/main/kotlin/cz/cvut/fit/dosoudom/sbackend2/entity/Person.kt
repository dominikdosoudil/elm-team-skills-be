package cz.cvut.fit.dosoudom.sbackend2.entity

import com.sun.istack.NotNull
import javax.persistence.*


@Entity
class Person (
    @NotNull
    var name: String,

    @ManyToMany
    @JoinTable(
        name = "person_skills",
        joinColumns = [JoinColumn(name = "person_id")],
        inverseJoinColumns = [JoinColumn(name = "skill_id")]
    )
    var skills: List<Skill> = listOf(),

    @ManyToMany(mappedBy = "persons")
    var projects: List<Project> = listOf(),

    @Id
    @GeneratedValue
    val id: Long? = null,
) {
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as Person

        if (name != other.name) return false
        if (skills != other.skills) return false
        if (id != other.id) return false

        return true
    }

    override fun hashCode(): Int {
        var result = name.hashCode()
        result = 31 * result + skills.hashCode()
        result = 31 * result + (id?.hashCode() ?: 0)
        return result
    }

}
