package cz.cvut.fit.dosoudom.sbackend2.repository

import cz.cvut.fit.dosoudom.sbackend2.entity.Project
import org.springframework.data.jpa.repository.JpaRepository

interface ProjectRepository : JpaRepository<Project, Long> {
    fun findByName(name: String): Project?
}